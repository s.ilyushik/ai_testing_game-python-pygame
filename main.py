import pygame
import math

from settings import *
from player import Player
from map import world_map


pygame.init()
sc = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption(WINDOW_CAPTION)
pygame.display.set_icon(pygame.image.load(WINDOW_ICO_PATH))
clock = pygame.time.Clock()

player = Player()

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
    player.movement()
    sc.fill(BLACK)
    pygame.draw.circle(sc, GREEN, (int(player.x), int(player.y)), 10)
    pygame.draw.line(
        sc, GREEN, player.player_current_position, (
            player.x + WIDTH * math.cos(player.angle),
            player.y + WIDTH * math.sin(player.angle)
        )
    )
    for x, y in world_map:
        pygame.draw.rect(sc, DARK_GRAY, (x, y, TILE, TILE), 2)
    pygame.display.update()
    clock.tick(FPS)
