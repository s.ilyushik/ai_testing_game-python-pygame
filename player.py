import pygame

from settings import *


class Player:
    """
    Main players class
    """
    def __init__(self):
        self.x, self.y = player_position
        self.angle = player_angle

    @property
    def player_current_position(self):
        return self.x, self.y

    def movement(self):
        """
        Track keystrokes and change corresponding attributes
        """
        keys = pygame.key.get_pressed()
        if keys[pygame.K_w]:
            self.y -= player_speed
        if keys[pygame.K_s]:
            self.y += player_speed
        if keys[pygame.K_a]:
            self.x -= player_speed
        if keys[pygame.K_d]:
            self.x += player_speed
        if keys[pygame.K_LEFT]:
            self.angle -= 0.02
        if keys[pygame.K_RIGHT]:
            self.angle += 0.02
