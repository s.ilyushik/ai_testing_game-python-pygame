# Screen
WIDTH = 800
HEIGHT = 520
HALF_WIDTH = WIDTH // 2
HALF_HEIGHT = HEIGHT //2
FPS = 60
TILE = 40

# Window attr
WINDOW_CAPTION = 'Easy game'
WINDOW_ICO_PATH = 'img/main_ico.bmp'

# Player start position
player_position = (HALF_WIDTH, HALF_HEIGHT)
player_angle = 0
player_speed = 2

# Colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 220, 0)
DARK_GRAY = (40, 40, 40)
